import axios from 'axios';
import mapboxgl from 'mapbox-gl';
// import 'mapbox-gl/dist/mapbox-gl.css';

const form = document.querySelector('form')!;
const addressInput = document.getElementById('address')! as HTMLInputElement;

type MapboxGeocodingResponse = {
    features: { geometry: { coordinates: [] } }[];
};

// declare var mapboxgl: any;

const searchAddressHandler = async (event: Event) => {
    event.preventDefault();

    const enteredAddress = addressInput.value;

    const url = `https://api.mapbox.com/geocoding/v5/mapbox.places/${enteredAddress}.json?access_token=${process.env.MAPBOX_API_KEY}`;

    try {
        const response = await axios.get<MapboxGeocodingResponse>(url);
        
        if (response.status !== 200) {
            throw new Error('Could not getch location');
        }

        const coordinates: number[] = response.data.features[0].geometry.coordinates;
        console.log('🚀 ~ file: app.ts ~ line 27 ~ searchAddressHandler ~ coordinates', coordinates);

        mapboxgl.accessToken = process.env.MAPBOX_API_KEY!;
        const map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [coordinates[0], coordinates[1]],
            zoom: 10
        });

        const marker = new mapboxgl.Marker()
            .setLngLat([coordinates[0], coordinates[1]])
            .addTo(map);
        
        map.addControl(new mapboxgl.NavigationControl());
        
        addressInput.value = '';
    } catch (error) {
        alert(error);
        console.error(error);
    }
}

form!.addEventListener('submit', searchAddressHandler);

